import execa = require('execa')
import DraftLog = require('draftlog')
import 'node-fetch'
import fetch, { FetchError, Response } from 'node-fetch'
DraftLog(console).addLineListener(process.stdin)
import * as fs from 'fs'
import * as path from 'path'
import { askQuestion } from './utils'
import * as settings from '../scripts/settings'

const regions = [
  //'us-east-1', // 5016
  //'af-south-1', // 856 //we had problems with this region in recent testing
  'ap-east-1', // 856
  'eu-central-1', // 1112
  //'eu-north-1', // 896
  'eu-south-1', // 856
  'eu-west-1', // 3000
  'eu-west-3', // 890
  'sa-east-1', // 3000
  'us-west-2', // 1112
]

const maxNodesPerRegion = {
  'us-east-1': 5016,
  'af-south-1': 856 ,
  'ap-east-1': 856,
  'eu-central-1': 1112,
  //'eu-north-1': 896,
  'eu-south-1': 856,
  'eu-west-1': 3000,
  'eu-west-3': 890,
  'sa-east-1': 3000,
  'us-west-2': 1112,
}


async function main() {
  // Confirm before PROD change
  await askQuestion("Are you sure you want to change the PRODUCTION network? ")
  await askQuestion("Are you absolutely sure? ")

  // Deploy SupportNodesStack, SupportNodesStackMonitor, or nothing depending on given args 
  /*
  let ArchiverIp = process.argv[4] || ''
  let MonitorIp = process.argv[5] || ''

  let result
  switch (true) {
    case (ArchiverIp === '' && MonitorIp === ''): {
      // Deploy SupportNodesStack
      const deploySupportNodes = execa.command('npx cdk deploy --require-approval never SupportNodesStack-default -o outputs/SupportNodesStack.out')
      if (deploySupportNodes.stdout) deploySupportNodes.stdout.pipe(process.stdout)
      if (deploySupportNodes.stderr) deploySupportNodes.stderr.pipe(process.stderr)
      result = await deploySupportNodes.catch(err => {
        console.warn(err)
        process.exit(1)
      })
      break

      // Parse results
    }
    case (ArchiverIp !== '' && MonitorIp === ''): {
      // Deploy SupportNodesStackMonitor
      const deploySupportNodesMonitor = execa.command('npx cdk deploy --require-approval never SupportNodesStackMonitor-default -o outputs/SupportNodesStackMonitor.out')
      if (deploySupportNodesMonitor.stdout) deploySupportNodesMonitor.stdout.pipe(process.stdout)
      if (deploySupportNodesMonitor.stderr) deploySupportNodesMonitor.stderr.pipe(process.stderr)
      result = await deploySupportNodesMonitor.catch(err => {
        console.warn(err)
        process.exit(1)
      })
      break

      // Parse results
    }
    case (ArchiverIp !== '' && MonitorIp !== ''): {
      // Dont deploy any support nodes
    }
  }

  // Parse result for outputs if any stacks were deployed
  if (result) {
    // Parse stderr(Weirdly has outputs...) from deploy
    const outputs = result && result.stderr ? result.stderr.match(/Outputs:[\w\W]+\n\n/gm) : []
    if (outputs === null) {
      console.warn('No outputs to parse')
      process.exit(1)
    }

    // Remove title and trailing blank lines
    const outputsArr = outputs !== null ? outputs[0].split('\n') : []
    outputsArr.shift()
    outputsArr.pop()
    outputsArr.pop()

    // Get Archiver and Monitor Ips
    for (const entry of outputsArr) {
      const [key, value] = entry.split(' = ')
      if (key.includes('MonitorIp')) {
        MonitorIp = value
      } else if (key.includes('ArchiverIp')) {
        ArchiverIp = value
      }
    }
  }

  // Output Monitor URL when its ready
  function pollServer (url: string, callback: (res?: Response) => void, attempts: number) {
    attempts -= 1
    if (attempts > 0) {
      fetch(url).then(callback).catch(_err => {
        setTimeout(() => {
          pollServer(url, callback, attempts)
        }, 2000);
      })
    } else {
      callback()
    }
  }

  pollServer(`http://${MonitorIp}:3000`, () => {
    console.log()
    console.log('\x1b[33m%s\x1b[0m', 'View network monitor at:') // Yellow
    console.log(`  http://${MonitorIp}:\x1b[32m%s\x1b[0m`, '3000') // Green
    console.log()
  }, 10)
  */

  const archiverCount = process.argv[4] || 1
  const existingArchivers = process.argv[5] || '[]'

  let result
  const deploySupportNodes = execa.command(`npx cdk deploy --require-approval never SupportNodesStack-eu-central-1 -o outputs/SupportNodesStack.out -c ArchiverCount=${archiverCount} -c ExistingArchivers=${existingArchivers}`)
  if (deploySupportNodes.stdout) deploySupportNodes.stdout.pipe(process.stdout)
  if (deploySupportNodes.stderr) deploySupportNodes.stderr.pipe(process.stderr)
  result = await deploySupportNodes.catch(err => {
    console.warn(err)
    process.exit(1)
  }) 

  let MonitorIp, ArchiverIp
  if (result) {
    // Parse stderr(Weirdly has outputs...) from deploy
    const outputs = result && result.stderr ? result.stderr.match(/Outputs:[\w\W]+\n\n/gm) : []
    if (outputs === null) {
      console.warn('No outputs to parse')
      process.exit(1)
    }

    // Remove title and trailing blank lines
    const outputsArr = outputs !== null ? outputs[0].split('\n') : []
    outputsArr.shift()
    outputsArr.pop()
    outputsArr.pop()

    // Get Archiver and Monitor Ips
    for (const entry of outputsArr) {
      const [key, value] = entry.split(' = ')
      if (key.includes('MonitorIp')) {
        MonitorIp = value
      } else if (key.includes('ArchiverIp')) {
        ArchiverIp = value
      }
    }
  }

  // Deploy the ConsensusNodesStack/s

  // Check args
  const appServerCount = Number(process.argv[2]) || 1
  const numOfRegions = Number(process.argv[3]) || 1
  
  // Sanity checks
  if (numOfRegions < 1) {
    console.warn(`Number of regions cannot be negative`)
    process.exit(1)
  }

  // Print archiver and monitor IP
  console.log(`
    Monitor: http://${MonitorIp}:3000/summary
    Archiver: http://${ArchiverIp}:4000/nodelist
  `)
  if (numOfRegions > regions.length) {
    console.warn(`Exceeded number of predefined regions: ${regions.length}`)
    process.exit(1)
  }
  // Get regional stack names
  const networkStacks = execa
    .commandSync('npx cdk list')
    .stdout.split('\n')
    .filter(name => name.includes('ConsensusNodesStack-'))
  const longest = networkStacks.reduce(function(a, b) {
    return a.length > b.length ? a : b
  })
  const maxLen = longest.length + 2 // for the ': '

  //build a string set of consensus stack names
  let availableStacks = new Set()
  for(let stack of networkStacks){
    availableStacks.add(stack)
  }

  const nodesPerStack = settings.NodesPerStack

  //make a list of used stacks so we can know what to delete later... //todo something that doesn't need this?
  var stream = fs.createWriteStream("stacks.txt", {flags:'w'});
  
  let totalDesiredNodes = appServerCount * numOfRegions
  let remainingNodesToDeploy = totalDesiredNodes
  let deployedNodes = 0
  let stackGroup = 0 

  const maxStacksForAnyRegion = 51 //should unhardcode this.  This is currently 5000/100 + 1
  let didSomething = true //helps us get out of this while loop if we run out of available stacks
  while(remainingNodesToDeploy > 0 && stackGroup <= maxStacksForAnyRegion && didSomething)
  {
    stackGroup++ //stack groups now start at "1" rather than "0"

    didSomething = false

    // Deploy stacks
    const promises = []
    let deployedThisGroup = 0
    for (let i = 0; i < numOfRegions; i++) {
      if(remainingNodesToDeploy <= 0){
        break
      }

      let region = regions[i]
      // we create the stack name based on where we are in the loop.
      const stack = `ConsensusNodesStack-${region}-${stackGroup}`

      //make sure this stack is available, if not skip to another region
      if(availableStacks.has(stack) === false){
        continue //region out of stacks let other regions take up the slack
      }
      //remove this stack from the available list
      availableStacks.delete(stack) //remove from available list
      stream.write(stack + ' ')

      //how many nodes to put in this stack?
      let nodesForThisStack = nodesPerStack
      if(nodesForThisStack > remainingNodesToDeploy){
        nodesForThisStack = remainingNodesToDeploy
      }
      //update some counts
      remainingNodesToDeploy -= nodesForThisStack
      deployedNodes += nodesForThisStack
      deployedThisGroup += nodesForThisStack

      //prepare the command line with out calculated nodesForThisStack
      const label = `${stack}: `.padStart(maxLen)
      const updateCmdOutput = console.draft(label + 'Starting to deploy...')
      let cmd = `npx cdk deploy --require-approval never ${stack} -o outputs/${stack}.out -c MonitorIp=${MonitorIp} -c ArchiverIp=${ArchiverIp}`
      cmd += ` -c AppServerCount=${nodesForThisStack}`
      cmd += ` -c RegionFilter=${region}`
      cmd += ` -c StackGroupFilter=${stackGroup}`

      const cmdProc = execa.command(cmd, { all: true })
      if (cmdProc.all) {
        const allBuffer = ''
        cmdProc.all.on('data', chunk => updateCmdOutput(getNewestLine(label, chunk, allBuffer)))
      }
      promises.push(cmdProc.catch(err => err))
      didSomething = true
    }
    //show a status on what this step did
    console.log(`deploying ${deployedThisGroup} nodes for this group ${stackGroup}.  ${deployedNodes} out of ${totalDesiredNodes}`)

    const results = await Promise.all(promises)
    console.log()
    for (const result of results) {
      console.log()
      console.log('======')
      if (result.all) console.log(result.all)
      else console.log(result)
      console.log('======')
      console.log()
    }
  }
  // Print archiver and monitor IP
  console.log(`
    Monitor: http://${MonitorIp}:3000/summary
    Archiver: http://${ArchiverIp}:4000/nodelist
  `)

  stream.end()

  process.exit()
}

main()

function getNewestLine(label: string, chunk: Buffer | string, buffer: string): string {
  // Save chunk to buffer
  buffer += chunk.toString()
  // Walk backwards through buffer until newline char
  let newLine = []
  let char
  const start = buffer.length - 1
  for (let i = start; i > -1; i--) {
    char = buffer[i]
    if (char === '\n' && i !== start) break
    else newLine.push(char)
  }
  // Return newest line
  const newestLine = label + newLine.reverse().join('')
  const maxLen = process.stdout.columns || 100
  if (newestLine.length > maxLen) {
    return newestLine.substr(0, maxLen - 5) + '...'
  } else {
    return newestLine
  }
}
