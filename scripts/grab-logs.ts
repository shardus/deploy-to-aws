import execa = require('execa')

const ip = process.argv[2]
const folder = `${ip}-logs`

async function main() {
  try {
    await execa('mkdir', [folder])
    const ssh = execa('ssh', ['-i', '~/.ssh/shardus-ent-cf.pem', `ec2-user@${ip}`, `docker cp \$(docker ps -q):/usr/src/app/logs -`])
    const tar = execa('tar', ['-C', folder, '-xv'])
    if (ssh.stdout && tar.stdin) {
      ssh.stdout.pipe(tar.stdin)
    }
    await Promise.all([ssh, tar])
  } catch (e) {
    console.log(e)
  }
}

main()
