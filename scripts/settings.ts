

export let NodesPerStack = 10
export let LargerVolumesForConsensusNodes = false
export let LargerVolumesForSuportNodes = false

export let ConsensusNodeSize = `t3.large`
export let ArchiverNodeSize = `t3.large`
export let MonitorNodeSize = `t3.large`

export let VirtualMemory = true

//Best to not oveerride these
export let ConsensusNodeMemory = -1
export let ArchiverNodeMemory = -1
export let MonitorNodeMemory = -1

const nodeJSMemoryPerInstanceSize : {[intance:string]:number} = {
    't3.micro': 500, //not sure this is viable without virtual memory
    't3.small': 800, 
    't3.medium': 2000,
    't3.large': 6000,
    't3.xlarge': 14000,
    't3.2xlarge': 16000, //it doesn't seem like nodeJS always likes to use more than 16
  }

function SetMemory(){

    if(nodeJSMemoryPerInstanceSize[ConsensusNodeSize] == null){
        throw Error(`invalid node size: ${ConsensusNodeSize}`)
    } 
    if(nodeJSMemoryPerInstanceSize[ArchiverNodeSize] == null){
        throw Error(`invalid node size: ${ArchiverNodeSize}`)
    } 
    if(nodeJSMemoryPerInstanceSize[MonitorNodeSize] == null){
        throw Error(`invalid node size: ${MonitorNodeSize}`)
    } 
    if(ConsensusNodeMemory === -1){
        ConsensusNodeMemory = nodeJSMemoryPerInstanceSize[ConsensusNodeSize]        
    }
    if(ArchiverNodeMemory === -1){
        ArchiverNodeMemory = nodeJSMemoryPerInstanceSize[ArchiverNodeSize]        
    }
    if(MonitorNodeMemory === -1){
        MonitorNodeMemory = nodeJSMemoryPerInstanceSize[MonitorNodeSize]        
    }

    console.log(`ConsensusNodeSize ${ConsensusNodeSize}`)
    console.log(`ArchiverNodeSize ${ArchiverNodeSize}`)
    console.log(`MonitorNodeSize ${MonitorNodeSize}`)
    
    console.log(`ConsensusNodeMemory ${ConsensusNodeMemory}`)
    console.log(`ArchiverNodeMemory ${ArchiverNodeMemory}`)
    console.log(`MonitorNodeMemory ${MonitorNodeMemory}`)
}

console.log(`NodesPerStack ${NodesPerStack}`)
console.log(`LargerVolumesForConsensusNodes ${LargerVolumesForConsensusNodes}`)
console.log(`LargerVolumesForSuportNodes ${LargerVolumesForSuportNodes}`)

console.log(`VirtualMemory ${VirtualMemory}\n`)

SetMemory()