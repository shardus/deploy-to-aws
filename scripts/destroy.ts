import execa = require('execa')
import DraftLog = require('draftlog')
import { askQuestion } from './utils'
DraftLog(console).addLineListener(process.stdin)

async function main() {
  // Confirm before PROD change
  await askQuestion("Are you sure you want to change the PRODUCTION network? ")
  await askQuestion("Are you absolutely sure? ")

  const args = process.argv.slice(2)

  switch (true) {
    // No args
    case args.length < 1: {
      // Delete Support and Consensus node stacks
      const allStacks = execa.commandSync('npx cdk list').stdout.split('\n')
      await deleteStacks(allStacks.filter((name) => name.includes('Support') || name.includes('Consensus')))
      break
    }
    // Single arg == 'all'
    case args.length < 2 && args[0] === 'all': {
      // Delete Support and Consensus node stacks first
      const allStacks = execa.commandSync('npx cdk list').stdout.split('\n')
      await deleteStacks(allStacks.filter((name) => name.includes('Support') || name.includes('Consensus')))
      // Delete RegionSetup stacks last
      await deleteStacks(allStacks.filter((name) => name.includes('RegionSetup')))
      break
    }
    // Single arg == 'all-regions'
    case args.length < 2 && args[0] === 'all-regions': {
      // Delete Support and Consensus node stacks first
      const allStacks = execa.commandSync('npx cdk list').stdout.split('\n')
      await deleteStacks(allStacks.filter((name) => name.includes('RegionSetup')))
      break
    }
    // Single arg == 'all-support'
    case args.length < 2 && args[0] === 'all-support': {
      // Delete Support and Consensus node stacks first
      const allStacks = execa.commandSync('npx cdk list').stdout.split('\n')
      await deleteStacks(allStacks.filter((name) => name.includes('Support')))
      break
    }
    // List of args
    default: {
      // Delete given list
      await deleteStacks(process.argv.slice(2))
    }
  }

  process.exit()
}

main()

async function deleteStacks(stacksToDelete: string[])  {
  if (stacksToDelete.length > 0) {
    const longest = stacksToDelete.reduce(function(a, b) {
      return a.length > b.length ? a : b
    })
    const maxLen = longest.length + 2 // for the ': '

    let deletePerUpdate = 10
    let totalToDelete = stacksToDelete.length
    let deleted = 0
    while(stacksToDelete.length > 0){
      let deleteThisTime = stacksToDelete.splice(0, deletePerUpdate)
      deleted+=deleteThisTime.length

      const promises = []
      for (const stack of deleteThisTime) {
        const label = `${stack}: `.padStart(maxLen)
        const updateCmdOutput = console.draft(label + 'Starting to destroy...')
        const cmdProc = execa.command(`npx cdk destroy --force -o outputs/${stack}.out ${stack}`, { all: true })
        if (cmdProc.all) {
          const allBuffer = ''
          cmdProc.all.on('data', chunk => updateCmdOutput(getNewestLine(label, chunk, allBuffer)))
        }
        promises.push(cmdProc.catch(err => err))
      }
      const results = await Promise.all(promises)
      console.log(`deleting ${deleted} out of ${totalToDelete}`)
      console.log()
      for (const result of results) {
        console.log()
        console.log('======')
        console.log(result.all ? result.all : result)
        console.log('======')
        console.log()
      }

    }
  } else {
    console.log('Nothing to delete')
  }
}

function getNewestLine(label: string, chunk: Buffer | string, buffer: string): string {
  // Save chunk to buffer
  buffer += chunk.toString()
  // Walk backwards through buffer until newline char
  let newLine = []
  let char
  const start = buffer.length - 1
  for (let i = start; i > -1; i--) {
    char = buffer[i]
    if (char === '\n' && i !== start) break
    else newLine.push(char)
  }
  // Return newest line
  const newestLine = label + newLine.reverse().join('')
  const maxLen = process.stdout.columns || 100
  if (newestLine.length > maxLen) {
    return newestLine.substr(0, maxLen - 5) + '...'
  } else {
    return newestLine
  }
}
