import cdk = require('@aws-cdk/core')
import ec2 = require('@aws-cdk/aws-ec2')
import ecs = require('@aws-cdk/aws-ecs')
import * as settings from '../scripts/settings'
import { DockerInstance } from './docker-instance-construct'

import { config } from 'dotenv'
import { resolve } from 'path'
import { getRegionVpcAndSG } from './utils'
config({ path: resolve(__dirname, '../.secrets') })

interface ConsensusNodesStackProps extends cdk.StackProps {
  region: string
}

export class ConsensusNodesStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: ConsensusNodesStackProps) {
    super(scope, id, props)

    // Get existing vpc and security group for the given region
    const { vpc, securityGroup } = getRegionVpcAndSG(props.region, this)

    const environment: any = {}
    environment[this.node.tryGetContext('AppSeedlistEnvVar')] = `${this.node.tryGetContext('ArchiverIp')}`
    environment[this.node.tryGetContext('AppMonitorEnvVar')] = `${this.node.tryGetContext('MonitorIp')}`
    environment[this.node.tryGetContext('AppIpAddrEnvVar')] = '`curl -s http://169.254.169.254/latest/meta-data/public-ipv4`'
    environment['NODE_OPTIONS'] = `--max-old-space-size=${settings.ConsensusNodeMemory}` //test 800mb for t3 micro/small.   medium=3584 IMPORTANT; set this to slightly less than the EC2 instance types memory
    environment['BASE_DIR'] = '/usr/src/app/config' // Hack that fixes issue with permissions when shardus try's to rename its old db upon restarting after being rotated out

    for (let i = 0; i < this.node.tryGetContext('AppServerCount'); i++) {
      let options = {
        imageUri: this.node.tryGetContext('AppImageUri'),
        username: process.env.AppRegistryUsername,
        token: process.env.AppRegistryToken,
        environment,
        dockerCmd: `docker run -Pd --restart=always --network='host' --mount type=bind,source=/home/ec2-user,target=/usr/src/app/logs --mount type=bind,source=/home/ec2-user,target=/usr/src/app/db --mount type=bind,source=/home/ec2-user,target=/usr/src/app/config`,
        instanceType: new ec2.InstanceType(settings.ConsensusNodeSize),
        machineImage: ecs.EcsOptimizedImage.amazonLinux2(),
        vpc,
        instanceName: 'validator',
        keyName: 'shardus-ent-cf',
        securityGroup,
        vpcSubnets: {
          subnetType: ec2.SubnetType.PUBLIC,
        },
      }
      if (settings.LargerVolumesForConsensusNodes) {
        //@ts-ignore
        options.blockDevices = [{
          deviceName: '/dev/xvda', // Use the root device name
          volume: ec2.BlockDeviceVolume.ebs(60), // Override the volume size in Gibibytes (GiB)
          mappingEnabled: true
        }]
      }
      const node = new DockerInstance(this, `NetworkNode${i}`, options)

      new cdk.CfnOutput(this, `NetworkNode${i}Ip`, {
        value: node.instancePublicIp,
      })
    }
  }
}
