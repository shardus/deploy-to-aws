import { Construct } from '@aws-cdk/core'
import * as settings from '../scripts/settings'
import ec2 = require('@aws-cdk/aws-ec2')
interface DockerInstanceProps extends ec2.InstanceProps {
  imageUri: string
  username?: string
  token?: string
  environment?: { [name: string]: string }
  dockerCmd?: string
}

export class DockerInstance extends ec2.Instance {
  constructor(scope: Construct, id: string, props: DockerInstanceProps) {
    super(scope, id, props)

    let env = ''
    if (props.environment) {
      env = Object.entries(props.environment)
        .map(([key, value]) => `--env ${key}=${value}`)
        .join(' ')
    }

    let dockerCmd = `docker run -Pd --restart=always --network='host'`
    if (props.dockerCmd) {
      dockerCmd = props.dockerCmd
    }

    let userData

    if (settings.VirtualMemory) {
      userData = [
        `docker pull ${props.imageUri}`,
        `${dockerCmd} ${env} ${props.imageUri}`,
        `systemctl stop rpcbind.service`, // Disables portmapper service for DDOS mitigation
        `systemctl stop rpcbind.socket`, // Disables portmapper service for DDOS mitigation
        `dd if=/dev/zero of=/swapfile bs=128M count=32`,  // Create 4 GB (128 MB * 32) swap file for virtual memory
        `chmod 600 /swapfile`,  // Sets permissions for swap file
        `mkswap /swapfile`,  // Sets up linux swap space
        `swapon /swapfile`, // Enables swap space
        `echo "/swapfile swap swap defaults 0 0" >> /etc/fstab` // Persists swap space after reboots
      ]
    } else {
      userData = [
        `docker pull ${props.imageUri}`,
        `${dockerCmd} ${env} ${props.imageUri}`,
        `systemctl stop rpcbind.service`,
        `systemctl stop rpcbind.socket`
      ]
    }

    if (props.username && props.token) {
      userData.unshift(
        `for i in 1 2 3 4 5; do docker login ${props.imageUri.split('/')[0]} -u ${props.username} -p ${props.token} && break || sleep 10; done`
      )
    }

    this.addUserData(...userData)
  }
}
