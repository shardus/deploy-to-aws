import { Construct } from "@aws-cdk/core";
import ec2 = require("@aws-cdk/aws-ec2");

export function getRegionVpcAndSG(region: string, construct: Construct) {
  let vpcName = ''
  let securityGroupId = ''
  switch (region) {
    case 'ap-east-1':
      vpcName = 'deploytoaws-ap-east-1-vpc'
      securityGroupId = 'sg-03da861004b554830'
      break
    case 'eu-central-1':
      vpcName = 'deploytoaws-eu-central-1-vpc'
      securityGroupId = 'sg-0d727a3ba7a57c5a4'
      break
    case 'eu-north-1':
      vpcName = 'deploytoaws-eu-north-1-vpc'
      securityGroupId = 'sg-0d9b8ccb16563ca08'
      break
    case 'eu-south-1':
      vpcName = 'deploytoaws-eu-south-1-vpc'
      securityGroupId = 'sg-08209bcbaeae8e308'
      break
    case 'eu-west-1':
      vpcName = 'deploytoaws-eu-west-1-vpc'
      securityGroupId = 'sg-02650caefcbdc8ab5'
      break
    case 'eu-west-3':
      vpcName = 'deploytoaws-eu-west-3-vpc'
      securityGroupId = 'sg-02fbbc9e7f3163f76'
      break
    case 'sa-east-1':
      vpcName = 'deploytoaws-sa-east-1-vpc'
      securityGroupId = 'sg-0f3d2f41502c8b96b'
      break
    case 'us-west-2':
      vpcName = 'deploytoaws-us-west-2-vpc'
      securityGroupId = 'sg-06a7724abc0dde7fd'
      break
    default:
      break
  }

  // Get the vpc for the given region
  const vpc = ec2.Vpc.fromLookup(construct, 'VPC', {
    vpcName
  });

  // Get the security group for the given region
  const securityGroup = ec2.SecurityGroup.fromSecurityGroupId(construct, 'SG', securityGroupId)

  return { vpc, securityGroup }
}