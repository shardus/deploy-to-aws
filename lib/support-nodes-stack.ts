import cdk = require('@aws-cdk/core')
import ec2 = require('@aws-cdk/aws-ec2')
import ecs = require('@aws-cdk/aws-ecs')
import * as settings from '../scripts/settings'
import { DockerInstance } from './docker-instance-construct'

import { config } from 'dotenv'
import { resolve } from 'path'
import { getRegionVpcAndSG } from './utils'
config({ path: resolve(__dirname, '../.secrets') })

interface SupportNodesStackProps extends cdk.StackProps {
  region: string
}

export class SupportNodesStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: SupportNodesStackProps) {
    super(scope, id, props)

    // Get existing vpc and security group for the given region
    const { vpc, securityGroup } = getRegionVpcAndSG(props.region, this)

    // Get ExistingArchivers
    const existingArchivers: string = this.node.tryGetContext('ExistingArchivers')

    const environment: any = {}
    environment[this.node.tryGetContext('ArchiverIpAddrEnvVar')] = '`curl -s http://169.254.169.254/latest/meta-data/public-ipv4`'
    if (existingArchivers && existingArchivers !== '') {
      environment['ARCHIVER_EXISTING'] = existingArchivers
    }
    environment['NODE_OPTIONS'] = `--max-old-space-size=${settings.ArchiverNodeMemory}`

    let archivers = []
    for (let i = 0; i < this.node.tryGetContext('ArchiverCount'); i++) {
      let archiverOptions = {
        imageUri: this.node.tryGetContext('ArchiverImageUri'),
        username: process.env.ArchiverRegistryUsername,
        token: process.env.ArchiverRegistryToken,
        environment,
        dockerCmd: `docker run -Pd --restart=always --network='host' --mount type=bind,source=/home/ec2-user,target=/usr/src/app/logs --mount type=bind,source=/home/ec2-user,target=/usr/src/app/db`,
        instanceType: new ec2.InstanceType(settings.ArchiverNodeSize),
        machineImage: ecs.EcsOptimizedImage.amazonLinux2(),
        vpc,
        instanceName: 'archiver',
        keyName: 'shardus-ent-cf',
        securityGroup,
        vpcSubnets: {
          subnetType: ec2.SubnetType.PUBLIC,
        },
      }
      if (settings.LargerVolumesForSuportNodes) {
        //@ts-ignore
        archiverOptions.blockDevices = [{
          deviceName: '/dev/xvda', // Use the root device name
          volume: ec2.BlockDeviceVolume.ebs(60), // Override the volume size in Gibibytes (GiB)
          mappingEnabled: true
        }]
      }

      archivers[i] = new DockerInstance(this, `Archiver${i}`, archiverOptions)
    }

    let monitorOptions = {
      imageUri: this.node.tryGetContext('MonitorImageUri'),
      username: process.env.MonitorRegistryUsername,
      token: process.env.MonitorRegistryToken,
      dockerCmd: `docker run -Pd --restart=always --network='host' --mount type=bind,source=/home/ec2-user,target=/usr/src/app/req-log --mount type=bind,source=/home/ec2-user,target=/usr/src/app/monitor-logs`,
      instanceType: new ec2.InstanceType(settings.MonitorNodeSize),
      machineImage: ecs.EcsOptimizedImage.amazonLinux2(),
      vpc,
      instanceName: 'monitor',
      keyName: 'shardus-ent-cf',
      securityGroup,
      vpcSubnets: {
        subnetType: ec2.SubnetType.PUBLIC,
      },
    }
    if (settings.LargerVolumesForSuportNodes) {
      //@ts-ignore
      monitorOptions.blockDevices = [{
        deviceName: '/dev/xvda', // Use the root device name
        volume: ec2.BlockDeviceVolume.ebs(60), // Override the volume size in Gibibytes (GiB)
        mappingEnabled: true
      }]
    }
    const monitor = new DockerInstance(this, 'Monitor', monitorOptions)

    if (archivers.length > 0) {
      new cdk.CfnOutput(this, 'ArchiverIp', {
        value: archivers[0].instancePublicIp,
      })
    }
    new cdk.CfnOutput(this, 'MonitorIp', {
      value: monitor.instancePublicIp,
    })
  }
}
