import cdk = require("@aws-cdk/core");
import ec2 = require("@aws-cdk/aws-ec2");
import ecs = require("@aws-cdk/aws-ecs");
import { DockerInstance } from "./docker-instance-construct";

import { resolve } from "path"
import { config } from "dotenv"
config({ path: resolve(__dirname, "../.secrets") })

interface SupportNodesStackMonitorProps extends cdk.StackProps {
  vpc?: ec2.Vpc
  securityGroup?: ec2.SecurityGroup
}

export class SupportNodesStackMonitor extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: SupportNodesStackMonitorProps) {
    super(scope, id, props);

    const vpc = props.vpc ? props.vpc : new ec2.Vpc(this, "VPC", {
      maxAzs: 1,
      subnetConfiguration: [
        {
          name: "Ingress",
          subnetType: ec2.SubnetType.PUBLIC
        }
      ]
    });

    const securityGroup = props.securityGroup ? props.securityGroup : new ec2.SecurityGroup(this, "SecruityGroup", {
      vpc
    });
    securityGroup.connections.allowFromAnyIpv4(ec2.Port.allTraffic());

    const monitor = new DockerInstance(this, "Monitor", {
      imageUri: this.node.tryGetContext("MonitorImageUri"),
      instanceType: new ec2.InstanceType("t3.micro"),
      machineImage: ecs.EcsOptimizedImage.amazonLinux2(),
      vpc,
      instanceName: "monitor-server",
      keyName: "shardus-ent-cf",
      securityGroup,
      vpcSubnets: {
        subnetType: ec2.SubnetType.PUBLIC
      }
    });

    new cdk.CfnOutput(this, "MonitorIp", {
      value: monitor.instancePublicIp
    });
  }
}
