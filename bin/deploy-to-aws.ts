#!/usr/bin/env node
import 'source-map-support/register'
import { ConsensusNodesStack } from '../lib/consensus-nodes-stack'
import { SupportNodesStack } from '../lib/support-nodes-stack'
import cdk = require('@aws-cdk/core')

const app = new cdk.App()

// List of all Amazon regions [2019-10-15]
/*
const regions = [
  'us-east-2', // ohio
  // "us-east-1",
  'us-west-1', // california
  // "us-west-2",
  // "ap-east-1",
  // "ap-south-1",
  // "ap-northeast-3",
  // "ap-northeast-2",
  // "ap-southeast-1",
  // "ap-southeast-2",
  'ap-northeast-1', // tokyo
  'ca-central-1', // canada
  // "cn-north-1",  Doesn't support env vars
  // "cn-northwest-1",  Doesn't support env vars
  // "eu-central-1",
  // "eu-west-1",
  'eu-west-2' // london
  // "eu-west-3",
  // "eu-north-1",
  // "me-south-1",
  // "sa-east-1",
  // "us-gov-east-1",
  // "us-gov-west-1"
]
*/

/*
US East (N. Virginia), us-east-1: 5016
US West (Oregon), us-west-2: 1112
Africa (Cape Town), af-south-1: 856
Asia Pacific (Hong Kong), ap-east-1: 856
EU (Frankfurt), eu-central-1: 1112
EU (Ireland), eu-west-1: 3000
EU (Milan), eu-south-1: 856
EU (Paris), eu-west-3: 890
EU (Stockholm), eu-north-1: 896
South America (Sao Paulo), sa-east-1: 3000
*/

// List of all Amazon regions [2021-01-06]
/*
const regions = [
  "us-east-1", // 5016
  "af-south-1", // 856
  "ap-east-1", // 856
  "ap-northeast-1",
  "ap-northeast-2",
  "ap-south-1",
  "ap-southeast-1",
  "ap-southeast-2",
  "ca-central-1",
  "eu-central-1", // 1112
  'eu-north-1', // 896
  'eu-south-1', // 856
  'eu-west-1', // 3000
  'eu-west-2',
  'eu-west-3', // 890
  'me-south-1',
  'sa-east-1', // 3000
  'us-east-2',
  'us-west-1',
  'us-west-2', // 1112
]
*/

// List of regions with extended limits [2021-08-26]
const regions = [
  //'us-east-1', // 5016
  //'af-south-1', // 856 //we had problems with this region in recent testing
  'ap-east-1', // 856
  'eu-central-1', // 1112
  //'eu-north-1', // 896
  'eu-south-1', // 856
  'eu-west-1', // 3000
  'eu-west-3', // 890
  'sa-east-1', // 3000
  'us-west-2', // 1112
]

const maxNodesPerRegion: { [region: string]: number } = {
  'us-east-1': 5016,
  'af-south-1': 856,
  'ap-east-1': 856,
  'eu-central-1': 1112,
  //'eu-north-1': 896,
  'eu-south-1': 856,
  'eu-west-1': 3000,
  'eu-west-3': 890,
  'sa-east-1': 3000,
  'us-west-2': 1112,
}


// Always launch support nodes in eu-central-1
new SupportNodesStack(app, 'SupportNodesStack-eu-central-1', { region: 'eu-central-1', env: { account: '806147424689', region: 'eu-central-1' } })

let stackGroupFilter = app.node.tryGetContext('StackGroupFilter') //will this call work here?
let regionFilter = app.node.tryGetContext('RegionFilter')

const maxNodesPerGroup = 100

// Define ConsensusNodesStacks for all regions
for (const region of regions) {

  if (regionFilter) {
    if (regionFilter != region) {
      continue
    }
  }

  let maxNodes = maxNodesPerRegion[region]
  let numStacks = Math.floor(maxNodes / maxNodesPerGroup)
  // if we want groups for remainder nodes can do this:
  // let remainder = maxNodes - numStacks * maxNodesPerGroup
  // if(remainder > 0){
  //   numStacks++
  // }
  for (let stackGroup = 1; stackGroup <= numStacks; stackGroup++) {
    if (stackGroupFilter) {
      if (Number(stackGroupFilter) != stackGroup) {
        continue
      }
    }

    new ConsensusNodesStack(app, `ConsensusNodesStack-${region}-${stackGroup}`, {
      region,
      env: { account: '806147424689', region },
    })
  }
}
