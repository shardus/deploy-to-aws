# Usage

To deploy to AWS, first make sure you have AWS credentials configured in your home directory as described here:

[AWS: Configuration and Credential Files](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

Next, create a `.secrets` file in this projects folder and add tokens to it:

`deploy-to-aws/.secrets`:
```
ArchiverRegistryUsername=<USERNAME>
ArchiverRegistryToken=<TOKEN>
AppRegistryUsername=<USERNAME>
AppRegistryToken=<TOKEN>
MonitorRegistryUsername=<USERNAME>
MonitorRegistryToken=<TOKEN>
```

If using the dev container, create a `.secrets-aws` file and add AWS credentials to it:

`deploy-to-aws/.secrets-aws`:
```
# AWS CLI credentials passed to dev container as environment variables
AWS_ACCESS_KEY_ID=<ACCESS_KEY_ID>
AWS_SECRET_ACCESS_KEY=<SECRET_ACCESS_KEY>
AWS_DEFAULT_REGION=eu-north-1
```

To view available stacks:

```
  npx cdk list
```

To deploy or destroy a single stack:

```
  npx cdk deploy <stack_name>

  npx cdk destroy <stack_name>
```

NPM scripts are provided to deploy and destroy Shardus networks on AWS:

```
  npm run deploy <nodes_per_region> <num_of_regions> [archiver_url] [monitor_url]

  npm run destroy [stack_name]  // Defaults to init/network stacks
```

# Development

- `npm run build` compile typescript to js
- `npm run watch` watch for changes and compile
- `npm run test` perform the jest unit tests
- `cdk deploy` deploy this stack to your default AWS account/region
- `cdk diff` compare deployed stack with current state
- `cdk synth` emits the synthesized CloudFormation template
