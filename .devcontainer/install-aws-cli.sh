#!/bin/sh
echo "Installing AWS CLI"
curl 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o 'awscliv2.zip'
unzip awscliv2.zip
sudo ./aws/install
echo "Cleaning AWS CLI install artifacts"
rm -rf ./aws ./awscliv2.zip